package com.application.service.impl;

import com.application.persistance.repository.ProductRepository;
import com.application.service.ProductService;
import com.application.service.dto.ProductDTO;
import com.application.service.dto.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
@Service
public class IProductService implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Collection<ProductDTO> findByAll() {
        return ProductMapper.entityToDTO(productRepository.findAll());
    }
}
