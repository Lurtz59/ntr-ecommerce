package com.application.service.impl;

import com.application.persistance.entity.Client;
import com.application.persistance.repository.ClientRepository;
import com.application.service.ClientService;
import com.application.service.dto.ClientDTO;
import com.application.service.dto.RegistrationDTO;
import com.application.service.dto.mapper.ClientMapper;
import com.application.service.dto.mapper.InscriptionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class IClientService implements ClientService {

    @Autowired
    private ISecurityService securityService;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public ClientDTO getLoggedUser() {
        final Client client = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ClientMapper.entityToDTO(client);
    }

    @Override
    public void autoLogin(String username, String password) {
        UserDetails userDetails = securityService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }
    }

    @Override
    public void registerNewClientAccount(RegistrationDTO inscriptionDTO) {
        inscriptionDTO.setPassword(bCryptPasswordEncoder.encode(inscriptionDTO.getPassword()));
        Client client = InscriptionMapper.dtoToClientEntity(inscriptionDTO);
        client.setEnabled(true);
        clientRepository.save(client);
    }

    @Override
    public boolean loginExist(String login) {
        return clientRepository.findByLogin(login) != null;
    }
}
