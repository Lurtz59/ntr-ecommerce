package com.application.service.dto.mapper;

import com.application.persistance.entity.Client;
import com.application.service.dto.ClientDTO;
import org.apache.commons.lang3.StringUtils;

public class ClientMapper {

    public static ClientDTO entityToDTO(Client client) {
        ClientDTO clientDTO = new ClientDTO();
        if(StringUtils.isNotBlank(client.getFirstName())) {
            clientDTO.setFirstName(client.getFirstName());
        }
        if(StringUtils.isNotBlank(client.getLastName())) {
            clientDTO.setLastName(client.getLastName());
        }
        return clientDTO;
    }
}
