package com.application.service.dto.mapper;

import com.application.persistance.entity.Product;
import com.application.service.dto.ProductDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class ProductMapper {

    public static ProductDTO entityToDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        if(product.getId() != null) {
            productDTO.setId(product.getId());
        }
        if(StringUtils.isNotBlank(product.getDescription())) {
            productDTO.setDescription(product.getDescription());
        }
        if(StringUtils.isNotBlank(product.getWording())) {
            productDTO.setWording(product.getWording());
        }
        if(product.getPrice() != null) {
            productDTO.setPrice(product.getPrice());
        }
        return productDTO;
    }

    public static Collection<ProductDTO> entityToDTO(Collection<Product> products) {
        Collection<ProductDTO> productDTO = new ArrayList<>();
        for(Product product : products) {
            productDTO.add(entityToDTO(product));
        }
        return productDTO;
    }
}
