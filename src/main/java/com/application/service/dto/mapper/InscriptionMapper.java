package com.application.service.dto.mapper;

import com.application.persistance.entity.Client;
import com.application.service.dto.RegistrationDTO;
import org.apache.commons.lang3.StringUtils;

public class InscriptionMapper {

    public static Client dtoToClientEntity(RegistrationDTO inscriptionDTO) {
        Client client = new Client();
        if(StringUtils.isNotBlank(inscriptionDTO.getFirstName())) {
            client.setFirstName(inscriptionDTO.getFirstName());
        }
        if(StringUtils.isNotBlank(inscriptionDTO.getLastName())) {
            client.setLastName(inscriptionDTO.getLastName());
        }
        if(StringUtils.isNotBlank(inscriptionDTO.getAddress())) {
            client.setAddress(inscriptionDTO.getAddress());
        }
        if(StringUtils.isNotBlank(inscriptionDTO.getLogin())) {
            client.setLogin(inscriptionDTO.getLogin());
        }
        if(StringUtils.isNotBlank(inscriptionDTO.getPassword())) {
            client.setPassword(inscriptionDTO.getPassword());
        }
        return client;
    }
}
