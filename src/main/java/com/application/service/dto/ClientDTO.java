package com.application.service.dto;

import java.io.Serializable;

public class ClientDTO implements Serializable {

    private String firstName;
    private String lastName;

    public ClientDTO() {}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
