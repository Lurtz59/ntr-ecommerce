package com.application.service;

import com.application.service.dto.ClientDTO;
import com.application.service.dto.RegistrationDTO;

public interface ClientService {

    ClientDTO getLoggedUser();

    void registerNewClientAccount(RegistrationDTO inscriptionDTO);

    boolean loginExist(String login);

    void autoLogin(String username, String password);
}
