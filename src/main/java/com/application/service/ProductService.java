package com.application.service;

import com.application.service.dto.ProductDTO;

import java.util.Collection;

public interface ProductService {

    Collection<ProductDTO> findByAll();
}
