package com.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CoffeeMachineStoreApplication {

	public static void main(String[] args) {
		final ApplicationContext context =   SpringApplication.run(CoffeeMachineStoreApplication.class, args);
	}
	
}
