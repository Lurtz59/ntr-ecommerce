package com.application.persistance.repository;

import com.application.persistance.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepostiory extends JpaRepository<Product, Integer> {
}
