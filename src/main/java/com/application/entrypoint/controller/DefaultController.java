package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.PageURL;
import org.springframework.web.bind.annotation.GetMapping;

public class DefaultController {

    @GetMapping(PageURL.ALL)
    public String defaultUrl() {
        return "redirect:"+ PageURL.HOME;
    }
}
