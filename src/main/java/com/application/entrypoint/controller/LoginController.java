package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.ErrorMessage;
import com.application.entrypoint.configuration.PageHTML;
import com.application.entrypoint.configuration.PageURL;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping(PageURL.LOGIN)
    public String login() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:"+ PageURL.HOME;
        }
        return PageHTML.LOGIN;
    }

    @GetMapping(PageURL.LOGIN_ERROR)
    public String login(final Model model) {
        model.addAttribute("errorMessage", ErrorMessage.BAD_CREDENTIAL);
        return PageHTML.LOGIN;
    }
}
