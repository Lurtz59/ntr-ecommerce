package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.PageHTML;
import com.application.entrypoint.configuration.PageURL;
import com.application.service.ClientService;
import com.application.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@Autowired
	ClientService clientService;

	@Autowired
	ProductService productService;

	@GetMapping(PageURL.HOME)
	public String home(final Model model) {
		model.addAttribute("user", clientService.getLoggedUser());
		model.addAttribute("products", productService.findByAll());
		return PageHTML.HOME;
	}
}
