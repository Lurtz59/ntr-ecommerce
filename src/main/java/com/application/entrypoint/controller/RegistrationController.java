package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.PageHTML;
import com.application.entrypoint.configuration.PageURL;
import com.application.service.ClientService;
import com.application.service.dto.RegistrationDTO;
import com.application.service.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

@Controller
public class RegistrationController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private RegistrationValidator registrationValidator;

    @GetMapping(PageURL.REGISTRATION)
    public String registration(WebRequest request, final Model model) {
        final RegistrationDTO registration = new RegistrationDTO();
        model.addAttribute("registration", registration);
        return PageHTML.REGISTRATION;
    }

    @PostMapping(PageURL.REGISTRATION)
    public String registerUserAccount(@ModelAttribute("registration") RegistrationDTO registration, BindingResult bindingResult) {
        registrationValidator.validate(registration, bindingResult);

        if (bindingResult.hasErrors()) {
            return PageHTML.REGISTRATION;
        }

        clientService.registerNewClientAccount(registration);
        clientService.autoLogin(registration.getLogin(), registration.getMatchingPassword());

        return "redirect:"+PageURL.HOME;
    }
}
