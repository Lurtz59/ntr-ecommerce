package com.application.entrypoint.configuration;

public interface PageURL {
    String ALL = "/";

    String REGISTRATION = "/inscription";

    String LOGIN = "/connexion";
    String LOGIN_ERROR = "/connexion-erreur";
    String LOGOUT = "/deconnexion";

    String HOME = "/accueil";
    String HOME_AUTHORIZATION = HOME +"/**";

    String PRODUCT = "/produit";
    String PRODUCT_AUTHORIZATION = PRODUCT +"/**";
    String BUY_PRODUCT = PRODUCT+"/acheter";

    String CSS = "/css/**";
    String JS = "/js/**";
    String WEBJARS = "/webjars/**";
}
