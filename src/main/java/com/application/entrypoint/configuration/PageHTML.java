package com.application.entrypoint.configuration;

public interface PageHTML {

    String HOME = "home";
    String LOGIN = "login";
    String REGISTRATION = "registration";
}
