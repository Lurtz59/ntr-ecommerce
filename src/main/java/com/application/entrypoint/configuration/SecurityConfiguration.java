package com.application.entrypoint.configuration;

import com.application.persistance.entity.type.RoleType;
import com.application.service.impl.ISecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {

	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";
	private static final int MAXIMUM_SESSIONS = 1;

	@Autowired
	private ISecurityService securityService;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();

		// The pages does not require login
		http.authorizeRequests().antMatchers(PageURL.ALL, PageURL.REGISTRATION, PageURL.LOGIN, PageURL.LOGIN_ERROR, PageURL.CSS, PageURL.JS, PageURL.WEBJARS).permitAll();

        // authorization users for those pages
		http.authorizeRequests().antMatchers(PageURL.HOME_AUTHORIZATION, PageURL.PRODUCT_AUTHORIZATION).hasRole(RoleType.CLIENT.name());

		// If no login, it will redirect to /login page.
		http.authorizeRequests().anyRequest().authenticated();

		// Config for Login Form
		http.authorizeRequests().and()
				.formLogin().loginPage(PageURL.LOGIN).defaultSuccessUrl(PageURL.HOME).failureUrl(PageURL.LOGIN_ERROR)
				.usernameParameter(LOGIN).passwordParameter(PASSWORD)
				.and()
				// Config for Logout Page
				.logout().invalidateHttpSession(true).logoutUrl(PageURL.LOGOUT).logoutSuccessUrl(PageURL.LOGIN);
		// config number session
		http.sessionManagement().maximumSessions(MAXIMUM_SESSIONS).expiredUrl(PageURL.LOGIN);
	}

	@Bean
	public AuthenticationManager customAuthenticationManager() throws Exception {
		return authenticationManager();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(securityService).passwordEncoder(bCryptPasswordEncoder());
	}
}
